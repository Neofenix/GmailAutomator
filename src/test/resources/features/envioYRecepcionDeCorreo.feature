Feature:Envío y recepción de correo electronico

  Como usuario de Gmail
  Quiero enviar y recibir correos electronicos
  De manera que pueda comunicarme con otras personas


  Background:
          Given que accedi al sitio de Gmail
          And  ingrese a la cuenta de correo
            |email                     |contrasena    |
            |remitentecorreo@gmail.com |Prueba123     |

  Scenario Outline: Envio de correo exitoso
  When redacto un correo electronico al "<emaildestinatario>" con asunto: "<asunto>" y cuerpo: "<cuerpo>"
  And envio el correo
  And el destinatario ingresa a la cuenta de correo: "<emaildestinatario>" y "<contrasenadestinatario>"
  Then el correo sera recibido correctamente

  Examples:
    |emaildestinatario                        |asunto    |cuerpo   |contrasenadestinatario             |
    |destinatarioprueba@gmail.com             |          |         |Prueba456                          |
    |destinatarioprueba@gmail.com             |aleatorio |aleatorio|Prueba456                          |
    |destinatarioprueba@gmail.com             |          |aleatorio|Prueba456                          |
    |destinatarioprueba@gmail.com             |aleatorio |         |Prueba456                          |



  Scenario Outline: Envio de correo fallido
    When redacto un correo electronico al "<emaildestinatario>" con asunto: "<asunto>" y cuerpo: "<cuerpo>"
    And envio el correo
    Then se debera mostrar la advertencia: "<mensaje>"

    Examples:
      |emaildestinatario                        |asunto    |cuerpo   |contrasenadestinatario             |mensaje                             |
      |                                         |aleatorio |aleatorio|Prueba456                          |Especifica al menos un destinatario.|
      |***?assdfsdf?#***                        |aleatorio |aleatorio|Prueba456                          |Asegúrate de que todas las direcciones tengan el formato correcto.|


