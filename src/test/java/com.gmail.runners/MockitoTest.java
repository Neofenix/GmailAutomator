package com.gmail.runners;

import com.gmail.data.Login;
import com.gmail.pages.CorreoPage;
import com.gmail.pages.CorreoRecibidoPage;
import com.gmail.pages.LoginPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;


public class MockitoTest {

    @Test
    public void testLogin() {
        Login login = new Login("jgzapataa@gmail.com", "prueba123");

        assertEquals("jgzapataa@gmail.com", login.getEmail());
        assertEquals("prueba123", login.getContrasena());

    }

    @Test
    public void testLoginPage() {

        LoginPage mockLoginPage = mock(LoginPage.class);
        List<Login> list = new LinkedList<>();
        List<Login> spy = spy(list);
        spy.add(mock(Login.class));
        doNothing().when(mockLoginPage).guardarDataCompartida(spy);

    }
}
