package com.gmail.runners;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/cucumber.json",
        detailedReport = true,
        detailedAggregatedReport = true,
        overviewReport = true,
        outputFolder = "target/EnvioDeCorreo")
@CucumberOptions(plugin = { "html:target/EnvioDeCorreo/cucumber-html-report",
        "json:target/cucumber.json", "pretty:target/cucumber-pretty.txt",
        "usage:target/cucumber-usage.json", "junit:target/cucumber-results.xml" },
        junit = {"--filename-compatible-names"},
        glue = {"classpath:com.gmail.util","com.gmail.steps","com.gmail.pages","com.gmail.definitions"},
        features = {"src/test/resources/features/"}
)

public class RunnerEnvioDeCorreo {
}

