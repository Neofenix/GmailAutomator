package com.gmail.definitions;

import com.gmail.data.Login;
import com.gmail.steps.GmailSteps;
import com.gmail.util.BaseTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static com.gmail.util.Constantes.URL_GMAIL;

public class GmailDefinitions {

    static WebDriver driver;
    private BaseTest basePage;


    @Given("^que accedi al sitio de Gmail$")
    public void dadoQueEstoyEnlaCuentaDeGMail() {
        basePage = new BaseTest();
        driver = basePage.iniciarNavegador(URL_GMAIL);
    }

    @When("^ingrese a la cuenta de correo$")
    public void ingresoALaCuentaDeCorreo(List<Login> datos) {
        GmailSteps gmailSteps = new GmailSteps(driver);
        gmailSteps.ingresarAlCorreo(datos);
    }

    @When("^el destinatario ingresa a la cuenta de correo: \"([^\"]*)\" y \"([^\"]*)\"$")
    public void ingresoALaCuentaDeCorreoDelDestinatario(String emaildestinatario, String contrasena) {
        driver.close();
        driver = basePage.iniciarNavegador(URL_GMAIL);
        Login login = new Login(emaildestinatario, contrasena);
        GmailSteps gmailSteps = new GmailSteps(driver);
        gmailSteps.ingresarAlCorreoDelDestinatario(login);
    }
}

