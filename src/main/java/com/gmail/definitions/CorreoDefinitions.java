package com.gmail.definitions;

import com.gmail.data.Correo;
import com.gmail.steps.CorreoSteps;
import com.gmail.util.DriverBase;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class CorreoDefinitions {

    private WebDriver driver;
    private CorreoSteps correoSteps;


    public CorreoDefinitions(DriverBase base) {
        this.driver = base.getDriver();
    }

    @When("^redacto un correo electronico (?:sin|al) \"([^\"]*)\" con asunto: \"([^\"]*)\" y cuerpo: \"([^\"]*)\"$")
    public void redactoUnCorreoElectronico(String destinatario, String asunto, String cuerpo) throws IOException {
        Correo correo = new Correo(destinatario, asunto, cuerpo);
        correoSteps = new CorreoSteps(driver);
        correoSteps.redactarCorreo(correo);
    }

    @When("^envio el correo$")
    public void envioElCorreoElectronico() throws IOException {
        correoSteps.envioCorreoElectronico();
    }

    @Then("^el correo sera recibido correctamente$")
    public void confirmaciondeEmailRecibido() throws IOException {
        correoSteps = new CorreoSteps(GmailDefinitions.driver);
        correoSteps.verificarCorreoEnviado();
    }

    @Then("^se debera mostrar la advertencia: \"([^\"]*)\"$")
    public void seDebeMostarElMensaje(String mensaje) throws IOException {
        correoSteps.validarAdvertenciaCorreoSinDestinatario(mensaje);
    }
}
