package com.gmail.steps;

import com.gmail.data.Correo;
import com.gmail.pages.CorreoPage;
import com.gmail.pages.CorreoRecibidoPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class CorreoSteps {

    private CorreoPage correoPage;

    public CorreoSteps(WebDriver driver) {
        correoPage = new CorreoPage(driver);
    }

    public void redactarCorreo(Correo datos) throws IOException {
        correoPage.seleccionarOpcionDeRedactar();
        correoPage.redactarCorreo(datos);
    }

    public void envioCorreoElectronico() throws IOException {
        correoPage.enviarCorreoElectronico();
    }


    public void verificarCorreoEnviado() throws IOException {
        CorreoRecibidoPage correoRecibidoPage = correoPage.seleccionarCorreo();
        Map<String, List<String>> lista = correoRecibidoPage.getDatosDeCorreo();
        Assert.assertEquals(lista.get("datosDeCorreoDestinatario"), lista.get("textoWebElementsCorreoRemitente"));
    }

    public void validarAdvertenciaCorreoSinDestinatario(String mensaje) throws IOException {
        correoPage.validarAdvertenciaCorreoSinDestinatario(mensaje);
    }

}
