package com.gmail.steps;

import com.gmail.data.Login;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class GmailSteps {

    private LoginSteps loginSteps;


    public GmailSteps(WebDriver driver) {
        loginSteps = new LoginSteps(driver);
    }

    public void ingresarAlCorreo(List<Login> datos) {
        loginSteps.guardarDataCompartida(datos);
        loginSteps.ingresarAGmail(datos);
    }

    public void ingresarAlCorreoDelDestinatario(Login datos) {
        List<Login> datosdestinatario = new ArrayList<>();
        datosdestinatario.add(datos);
        loginSteps.ingresarAGmail(datosdestinatario);
    }


}
