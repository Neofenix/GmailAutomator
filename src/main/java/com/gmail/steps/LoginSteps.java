package com.gmail.steps;

import com.gmail.data.Login;
import com.gmail.pages.CorreoPage;
import com.gmail.pages.LoginPage;
import org.openqa.selenium.WebDriver;

import java.util.List;


public class LoginSteps {

    private LoginPage loginPage;

    public LoginSteps(WebDriver driver) {
        loginPage = new LoginPage(driver);
    }

    public void ingresarAGmail(List<Login> datos) {
        CorreoPage correoPage = loginPage.login(datos);
        correoPage.estoyEnLaBandeja();
    }

    public void guardarDataCompartida(List<Login> datos) {
        loginPage.guardarDataCompartida(datos);
    }

}
