package com.gmail.pages;

import com.gmail.data.DatoCorreoDestinatario;
import com.gmail.util.ApoyoUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CorreoRecibidoPage extends ApoyoUtil {

    @FindBy(className = "hP")
    @CacheLookup
    public WebElement asunto;

    @FindBy(className = "g2")
    @CacheLookup
    public WebElement destinatario;

    @FindBy(className = "gD")
    @CacheLookup
    public WebElement remitente;

    @FindBy(xpath = ".//div[@class='ii gt adP adO']/div")
    @CacheLookup
    public WebElement cuerpo;

    private List<String> txtWebElementsCorreoRemitente = Arrays.asList(asunto.getText(), remitente.getAttribute("email"), destinatario.getAttribute("email"), cuerpo.getText());
    private List<String> datosCorreoDestinatario = Arrays.asList(DatoCorreoDestinatario.asuntoRemitente.get(), DatoCorreoDestinatario.correoRemintente.get(), DatoCorreoDestinatario.destinatario.get(), DatoCorreoDestinatario.cuerpoRemitente.get());

    public CorreoRecibidoPage(WebDriver driver) {
        super(driver);
    }

    public Map<String, List<String>> getDatosDeCorreo() {
        Map<String, List<String>> map = new HashMap();
        map.put("textoWebElementsCorreoRemitente", txtWebElementsCorreoRemitente);
        map.put("datosDeCorreoDestinatario", datosCorreoDestinatario);
        return map;
    }

}
