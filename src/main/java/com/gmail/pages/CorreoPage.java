package com.gmail.pages;

import com.gmail.data.Correo;
import com.gmail.data.DatoCorreoDestinatario;
import com.gmail.util.ApoyoUtil;
import com.gmail.util.Constantes;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

public class CorreoPage extends ApoyoUtil {

    @FindBy(xpath = "*//div[contains(text(), 'REDACTAR')]")
    @CacheLookup
    public WebElement bttonRedactar;

    @FindBy(name = "to")
    @CacheLookup
    public WebElement txtDestinatario;

    @FindBy(name = "subjectbox")
    @CacheLookup
    public WebElement txtAsunto;

    @FindBy(xpath = ".//div[@aria-label='Cuerpo del mensaje']")
    @CacheLookup
    public WebElement txtCuerpo;

    @FindBy(className = "vh")
    @CacheLookup
    public WebElement lblConfirmacion;

    @FindBy(className = "Kj-JD-Jz")
    @CacheLookup
    public WebElement lblMensajeError;

    private String cadenaAleatoria;

    public CorreoPage(WebDriver driver) {
        super(driver);
    }

    public void estoyEnLaBandeja() {
        elementoEsVisible(bttonRedactar);
    }

    public void seleccionarOpcionDeRedactar() {
        esperarElemento(bttonRedactar);
        bttonRedactar.click();

    }

    public void redactarCorreo(Correo datos) throws IOException {
        teclear(txtDestinatario, datos.getDestinatario());
        DatoCorreoDestinatario.destinatario.set(datos.getDestinatario());
        redactarAsunto(datos);
        redactarCuerpo(datos);
        tomarPantallazo();
    }

    private void redactarAsunto(Correo datos) {
        if (datos.getAsunto().isEmpty()) {
            DatoCorreoDestinatario.asuntoRemitente.set(Constantes.ASUNTO_VACIO);
        } else if (datos.getAsunto().equals(Constantes.ALEATORIO)) {
            cadenaAleatoria = cadenaAlfanumericaAleatoria(Constantes.VEINTE);
            teclear(txtAsunto, cadenaAleatoria);
            DatoCorreoDestinatario.asuntoRemitente.set(cadenaAleatoria);
        } else {
            teclear(txtAsunto, datos.getAsunto());
            DatoCorreoDestinatario.asuntoRemitente.set(datos.getAsunto());
        }
    }

    private void redactarCuerpo(Correo datos) {
        if (datos.getCuerpo().equals(Constantes.ALEATORIO)) {
            cadenaAleatoria = cadenaAlfanumericaAleatoria(Constantes.VEINTE);
            teclear(txtCuerpo, cadenaAleatoria);
            DatoCorreoDestinatario.cuerpoRemitente.set(cadenaAleatoria);
        } else {
            teclear(txtCuerpo, datos.getCuerpo());
            DatoCorreoDestinatario.cuerpoRemitente.set(datos.getCuerpo());
        }
    }

    public void enviarCorreoElectronico() throws IOException {
        String enviarCorreo = Keys.chord(Keys.CONTROL, Keys.RETURN);
        if (DatoCorreoDestinatario.asuntoRemitente.get().equals(Constantes.ASUNTO_VACIO) && DatoCorreoDestinatario.cuerpoRemitente.get().isEmpty()) {
            teclear(txtCuerpo, enviarCorreo);
            aceptarAlertarDelNavegador();
            validarEnvioDeCorreo();
            tomarPantallazo();
        } else {
            teclear(txtCuerpo, enviarCorreo);
            if (!DatoCorreoDestinatario.destinatario.get().isEmpty() && DatoCorreoDestinatario.destinatario.get().contains(Constantes.SIMBOLO_ARROBA)) {
                validarEnvioDeCorreo();
                tomarPantallazo();
            }
        }
    }

    public void validarEnvioDeCorreo() {
        esperarElemento(lblConfirmacion);
        elementoEsVisible(lblConfirmacion, Constantes.MENSAJE_ENVIADO);
    }

    public CorreoRecibidoPage seleccionarCorreo() throws IOException {
        driver.findElement(By.cssSelector(Constantes.SELECTOR_OPCION_RECIBIDOS));
        return seleccionarPrimerCorreo(buscarCorreosNoLeidos(), DatoCorreoDestinatario.asuntoRemitente.get());
    }


    public void validarAdvertenciaCorreoSinDestinatario(String mensaje) throws IOException {
        esperarElemento(lblMensajeError);
        tomarPantallazo();
        validarInformacion(lblMensajeError.getText(), mensaje);
    }
}
