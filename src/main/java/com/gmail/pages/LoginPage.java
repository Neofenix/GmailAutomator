package com.gmail.pages;

import com.gmail.data.DatoCorreoDestinatario;
import com.gmail.data.Login;
import com.gmail.util.ApoyoUtil;
import com.gmail.util.Constantes;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class LoginPage extends ApoyoUtil {


    @FindBy(how = How.ID, using = "identifierId")
    @CacheLookup
    public WebElement email;
    @FindBy(how = How.NAME, using = "password")
    @CacheLookup
    public WebElement contrasena;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public CorreoPage login(List<Login> datos) {

        for (Login datosLogeo : datos) {
            email.sendKeys(datosLogeo.getEmail());
            email.sendKeys(Keys.ENTER);
            esperarElemento(contrasena);
            contrasena.sendKeys(datosLogeo.getContrasena());
            contrasena.sendKeys(Keys.ENTER);
        }
        return new CorreoPage(driver);
    }


    public void guardarDataCompartida(List<Login> datos) {
        try {
            String emailRemitente = datos.get(Constantes.POSICION_EMAIL_REMITENTE).getEmail();
            DatoCorreoDestinatario.correoRemintente.set(emailRemitente);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
