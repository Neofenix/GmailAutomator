package com.gmail.util;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.io.IOException;

public class Hook extends DriverBase {


    @Before
    public void inicializarAmbiente(Scenario scenario) throws IOException {
        Evidencia.setNombreCarpeta(scenario.getName());

    }

    @After
    public void finalizarAmbiente() {
        getDriver().quit();

    }

}
