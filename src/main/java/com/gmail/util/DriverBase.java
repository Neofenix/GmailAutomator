package com.gmail.util;

import org.openqa.selenium.WebDriver;


public class DriverBase {
    private static WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        DriverBase.driver = driver;
    }

}