package com.gmail.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;


public class BaseTest extends DriverBase {

    public WebDriver iniciarNavegador(String url) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--incognito");
        ChromeDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Constantes.TIEMPO_DE_ESPERA_IMPLICITO, TimeUnit.SECONDS);
        driver.navigate().to(url);
        setDriver(driver);
        return driver;
    }
}
