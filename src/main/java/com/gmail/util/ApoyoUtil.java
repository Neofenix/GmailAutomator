package com.gmail.util;

import com.gmail.pages.CorreoRecibidoPage;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;

import static com.gmail.util.Evidencia.contador;
import static com.gmail.util.Evidencia.nombreCarpeta;

public class ApoyoUtil {


    protected WebDriver driver;


    public ApoyoUtil(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void elementoEsVisible(WebElement elemento) {
        Assert.assertTrue("No es visible el elemento", elemento.isDisplayed());
    }

    public void elementoEsVisible(WebElement textoWebElement, String mensaje) {
        MatcherAssert.assertThat(textoWebElement.getText(), Matchers.containsString(mensaje));
    }

    public void validarInformacion(String txtWebElement, String elemento) {
        MatcherAssert.assertThat(txtWebElement, Matchers.containsString(elemento));
    }


    public void esperarElemento(WebElement elemento) {
        WebDriverWait wait = new WebDriverWait(driver, Constantes.TIEMPO_DE_ESPERA_ELEMENTO);
        wait.until(ExpectedConditions.visibilityOf(elemento));
    }

    public void teclear(WebElement elemento, CharSequence accion) {
        elemento.sendKeys(accion);
    }

    public List<WebElement> buscarCorreosNoLeidos() {
        return driver.findElements(By.cssSelector(Constantes.SELECTOR_CORREOS_NO_LEIDOS));
    }

    public CorreoRecibidoPage seleccionarPrimerCorreo(List<WebElement> webElements, String asuntoDestinatario) throws IOException {
        for (WebElement asunto : webElements) {
            if (asunto.getText().equals(asuntoDestinatario)) {
                asunto.isDisplayed();
                asunto.click();
                break;
            }

        }
        tomarPantallazo();
        return new CorreoRecibidoPage(driver);
    }

    public void aceptarAlertarDelNavegador() {
        WebDriverWait wait = new WebDriverWait(driver, Constantes.TIEMPO_DE_ESPERA_ELEMENTO);
        wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
    }


    public String cadenaAlfanumericaAleatoria(int count) {
        StringBuilder builder = new StringBuilder();
        int contador = count;
        while (contador-- != 0) {
            int character = (int) (Math.random() * Constantes.CADENA_ALFANUMERICA.length());
            builder.append(Constantes.CADENA_ALFANUMERICA.charAt(character));
        }
        return builder.toString();
    }

    public void tomarPantallazo() throws IOException {
        Evidencia evidencia = new Evidencia();
        evidencia.capturarFotoPantalla("" + contador, nombreCarpeta, driver);
        contador++;

    }
}
