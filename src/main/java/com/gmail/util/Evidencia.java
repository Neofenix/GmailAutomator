package com.gmail.util;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class Evidencia {


    static String nombreCarpeta;
    static int contador = 1;

    public static void setNombreCarpeta(String carpeta) {
        int[] resultado = getfechaHoySeparada();
        nombreCarpeta = carpeta + "-" + resultado[Constantes.FECHA_ACTUAL_ANIO] + "-"
                + resultado[Constantes.FECHA_ACTUAL_MES] + "-"
                + resultado[Constantes.FECHA_ACTUAL_DIA] + "T"
                + resultado[Constantes.FECHA_ACTUAL_HORA] + "-"
                + resultado[Constantes.FECHA_ACTUAL_MINUTO] + "-"
                + resultado[Constantes.FECHA_ACTUAL_SEGUNDO];
    }


    public static int[] getfechaHoySeparada() {
        Calendar cal = Calendar.getInstance();
        int[] resultado = new int[6];
        resultado[Constantes.FECHA_ACTUAL_ANIO] = cal.get(Calendar.YEAR);
        resultado[Constantes.FECHA_ACTUAL_MES] = (cal.get(Calendar.MONTH)) + 1;
        resultado[Constantes.FECHA_ACTUAL_DIA] = cal.get(Calendar.DATE);
        resultado[Constantes.FECHA_ACTUAL_HORA] = cal.get(Calendar.HOUR);
        resultado[Constantes.FECHA_ACTUAL_MINUTO] = cal.get(Calendar.MINUTE);
        resultado[Constantes.FECHA_ACTUAL_SEGUNDO] = cal.get(Calendar.SECOND);
        return resultado;
    }

    public void capturarFotoPantalla(String imageName, String carpeta, WebDriver driver) throws IOException {
        String carpetaEvidencias = "Evidencia";
        File directory = new File(new File(".").getCanonicalPath().concat("\\") + carpetaEvidencias.concat("\\") + carpeta);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        try {
            if (directory.isDirectory()) {
                // Toma la captura de imagen
                WebDriver augmentedDriver = new Augmenter().augment(driver);
                File imagen = ((TakesScreenshot) augmentedDriver)
                        .getScreenshotAs(OutputType.FILE);
                // Mueve el archivo a la carga especificada con el respectivo nombre
                FileUtils.copyFile(imagen, new File(directory.getAbsolutePath().concat("\\")
                        + imageName + ".png"));
            } else {
                // Se lanza la excepcion cuando no encuentre el directorio
                throw new IOException(
                        "ERROR : La ruta especificada no es un directorio!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
