package com.gmail.util;

public class Constantes {

    public static final String CADENA_ALFANUMERICA = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
    public static final String ALEATORIO = "aleatorio";
    public static final String ASUNTO_VACIO = "(sin asunto)";
    public static final String MENSAJE_ENVIADO = "Tu mensaje ha sido enviado";
    public static final int TIEMPO_DE_ESPERA_IMPLICITO = 15;
    public static final int TIEMPO_DE_ESPERA_ELEMENTO = 6;
    public static final String URL_GMAIL = "https://www.gmail.com";
    public static final int POSICION_EMAIL_REMITENTE = 0;
    public static final int VEINTE = 20;
    public static final String SIMBOLO_ARROBA = "@";
    public static final String SELECTOR_CORREOS_NO_LEIDOS = "div.xT>div.y6>span>b";
    public static final String SELECTOR_OPCION_RECIBIDOS = ".nU.n1";


    	/*
     * Lista de opciones del metodo getFechaHoySeparada
	 */

    public static final int FECHA_ACTUAL_ANIO = 0;
    public static final int FECHA_ACTUAL_MES = 1;
    public static final int FECHA_ACTUAL_DIA = 2;
    public static final int FECHA_ACTUAL_HORA = 3;
    public static final int FECHA_ACTUAL_MINUTO = 4;
    public static final int FECHA_ACTUAL_SEGUNDO = 5;


    private Constantes() {
        throw new UnsupportedOperationException();
    }
}
