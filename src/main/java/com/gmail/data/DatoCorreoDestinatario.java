package com.gmail.data;

public class DatoCorreoDestinatario {

    public static ThreadLocal<String> correoRemintente;
    public static ThreadLocal<String> destinatario;
    public static ThreadLocal<String> asuntoRemitente;
    public static ThreadLocal<String> cuerpoRemitente;

    static {
        correoRemintente = new ThreadLocal<>();
        destinatario = new ThreadLocal<>();
        asuntoRemitente = new ThreadLocal<>();
        cuerpoRemitente = new ThreadLocal<>();
    }

    private DatoCorreoDestinatario() {
    }
}
